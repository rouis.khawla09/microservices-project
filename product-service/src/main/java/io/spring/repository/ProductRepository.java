package io.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.spring.model.Product;

public interface ProductRepository extends JpaRepository<Product, Long> {

}
