package io.spring.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.spring.dto.ProductRequest;
import io.spring.dto.ProductResponse;
import io.spring.model.Product;
import io.spring.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;

	public void createProduct(ProductRequest productRequest) {
		Product product = Product.builder()
				.name(productRequest.getName())
				.description(productRequest.getDescription())
				.price(productRequest.getPrice())
				.build();
	    productRepository.save(product);
	}

	public List<ProductResponse> getProducts() {
		List<Product> products = productRepository.findAll();
		return products.stream().map(this::mapToProductResponse).collect(Collectors.toList());
		}
	
	private ProductResponse mapToProductResponse(Product product) {
		return ProductResponse.builder()
				.id(product.getId())
				.name(product.getName())
				.price(product.getPrice())
				.description(product.getDescription())
				.build();
	}

}
