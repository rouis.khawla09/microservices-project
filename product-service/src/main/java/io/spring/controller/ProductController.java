package io.spring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import io.spring.dto.ProductRequest;
import io.spring.dto.ProductResponse;
import io.spring.service.ProductService;

@RestController
@RequestMapping("/api/product")
public class ProductController {
	
	@Autowired
	ProductService productService;

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping
	public void createProduct(@RequestBody ProductRequest productRequest) {
	    productService.createProduct(productRequest);
	}
	
	@ResponseStatus(HttpStatus.OK)
	@GetMapping
	public List<ProductResponse> getProducts(){
	    return productService.getProducts();
	}

	
}
