package io.spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.spring.dto.InventoryResponse;
import io.spring.model.Inventory;
import io.spring.repository.InventoryRepository;

@Service
public class InventoryService {
	
	@Autowired
	private InventoryRepository inventoryRepo;
	
	@Transactional(readOnly=true)
	public Boolean isInStock(String code) {
		Inventory inventory = inventoryRepo.findByCode(code).get();
		InventoryResponse inventoryResponse = InventoryResponse.builder()
		          .code(inventory.getCode())
		          .isInStock(inventory.getQuantity() > 0)
		          .build();
		return inventoryResponse.isInStock();
	}

}
