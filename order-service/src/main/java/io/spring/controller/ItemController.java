package io.spring.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import io.spring.model.Item;
import io.spring.service.ItemService;
import io.spring.service.OrderService;

@RequestMapping("/api/orders")
@RestController
public class ItemController {
	
	@Autowired
	ItemService itemService;
	@Autowired
	OrderService orderService;
	
	@GetMapping("/{orderId}/items")
	public List<Item> getAllItems(@PathVariable Long orderId) {
		return itemService.getAllItems(orderId);
	}
	
	@GetMapping("/items/{id}")
	public Item getItem(@PathVariable Long id) {
		return itemService.getItem(id);
	}


}
