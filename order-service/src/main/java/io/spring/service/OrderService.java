package io.spring.service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.reactive.function.client.WebClient;

import io.spring.dto.ItemDto;
import io.spring.dto.OrderRequest;
import io.spring.model.Item;
import io.spring.model.Order;
import io.spring.repository.OrderRepository;

@Service
@Transactional
public class OrderService {
	
	@Autowired
	private OrderRepository orderRepo;
	@Autowired
	private WebClient webClient;
	
	public Order addOrder(OrderRequest orderRequest) {
		Order order = new Order();
		order.setOrderNumber(UUID.randomUUID().toString());
		List<Item> items = orderRequest.getItemDtoList()
				.stream()
				.map(this::mapToDto)
				.collect(Collectors.toList());
		order.setItems(items);

		return orderRepo.save(order);
	}

	private Item mapToDto(ItemDto itemDto) {
		Item item = new Item();
		item.setPrice(itemDto.getPrice());
		item.setCode(itemDto.getCode());
		item.setQuantity(itemDto.getQuantity());
		return item;
	}
	
}
