package io.spring.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import io.spring.model.Item;
import io.spring.model.Order;
import io.spring.repository.ItemRepository;
import io.spring.repository.OrderRepository;

@Service
public class ItemService {

	@Autowired
	private ItemRepository itemRepo;
	@Autowired
	private OrderRepository orderRepo;
	
	public List<Item> getAllItems(Long orderId) {
		 Order o = orderRepo.findById(orderId).get();    
		 return o.getItems();
	}
	
	public Item getItem(Long id) {
		 return itemRepo.findById(id).get();
	}
	
	
	
	
}
