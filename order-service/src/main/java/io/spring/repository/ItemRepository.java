package io.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.spring.model.Item;

public interface ItemRepository  extends JpaRepository<Item, Long> {
	
}
