package io.spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import io.spring.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long> {

}
